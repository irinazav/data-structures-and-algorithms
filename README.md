﻿# **Data Structures and Algorithms**

--------------------------  
## **Binary Tree **

* Tree traversals: inorder, preorder and postorder (recursive and iterative)
* Tree level (queue-based and recursive)
* Add and remove value
* Min and max value
* Min diffrence for value
* If value exists?
* If tree balanced?

![BTree.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/BTree.jpg)  
 
    
--------------------------  
  
## **Linked List**

* Deep copy
* Insert a node (at the tail, head, nth index)
* Delete node by value
* Remove dublicates
* Reverse list (recursive)
* Nth element (recursive and iterative)
* Middle node (recursive and iterative)
* Print in reverse order(recursive)
* Is Polindrome?
* Rotate nodes
* Interweave list
* Detect cycle
* Find merge point of T(Y) list
* Intersection of two lists 
* Merge two sorted lists 

![LinkedList.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/LinkedList.jpg)

--------------------------  
## **Sort and Search**

* [Radix sort](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/40d441b720a0bc3dc3c0fb8b1853790796e1ade2/SortAndSearch/RadixSort/RadixSort/?at=master)

![RadixSort.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/RadixSort.jpg)

* Bubble sort
* Insertion sort
* Selection sort
* Quick sort
* Merge sort
* Binary search

![SortAndSearch.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/SortAndSearch.jpg)

--------------------------  
## **Array**
* [K'th largest element in Unsorted Array](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayKthLargest/ArrayKthLargest/?at=master)

![ArrayKthLargest.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayKthLargest.jpg)



* [Find first occurrences of an element in a sorted array](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayFindFirstIndex/ArrayFindFirstIndex/?at=master) 

![ArrayFindFirstIndex.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayFindFirstIndex.jpg)



* [Finding the balance point in an array.](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayBalancePoint/?at=master)   (The index where the  sum of the elements to the left it is the same as the sum of the elements to the right of it)

![ArrayBalancePoint.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayBalancePoint.jpg)



* [Find the number of occurrences of a number in a given sorted array](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayNumberOccurrence/ArrayNumberOccurrence/?at=master)

![ArrayNumberOccurrence.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayNumberOccurrence.jpg)



* [Print All the Subsets of a Given Set (Power Set)](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayPrintAllSumPairsForNumber/ArrayPrintAllSumPairsForNumber/?at=master)

![ArrayPrintAllSubsets.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayPrintAllSubsets.jpg)


* [Count pairs with given sum](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayPrintAllSumPairsForNumber/ArrayPrintAllSumPairsForNumber/?at=master)

![ArrayPrintAllSumPairsForNumber.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayPrintAllSumPairsForNumber.jpg)


* [Kadane’s algorithm — Maximum subarray](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayMaxSubArrayKadane/ArrayMaxSubArrayKadane/?at=master) 

![ArrayKadane.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayKadane.jpg)



* [Find longest increasing sequence (LIS)](https://bitbucket.org/irinazav/data-structures-and-algorithms/src/9210ce909e7ea0326f001ba8e630782af8212c38/Array/ArrayLIS/ArrayLIS/?at=master)

![ArrayLIS.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/ArrayLIS.jpg)


--------------------------  
## **Math**

* Sqrt

![SQRT.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/SQRT.jpg)


