﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayFindFirstIndex
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums1 = { 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9 };
            Console.WriteLine("Array:  {1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9}");	
            Console.WriteLine("First index of number 3 is " + GetFirstIndex(nums1, 3, 0, nums1.Length - 1));
            Console.WriteLine();

            int[] nums2 = { 0, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 6, 8, 8, 9 };
            Console.WriteLine("Array:  { 0, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 6, 8, 8, 9 }");
            Console.WriteLine("First index of number 4 is " + GetFirstIndex(nums2, 4, 0, nums2.Length - 1));
            Console.ReadKey();
        }

        public static int GetFirstIndex(int[] nums, int a, int start, int end)
        {            
            if (end < start) return -1;          
            if (nums[start] > a) return -1;
            if (nums[end] < a) return -1;             
            if (nums[start] == a) return start;           
            int mid = (start + end) / 2;

            if (nums[mid] == a)
            {              
                int leftIndex = GetFirstIndex(nums, a, start, mid - 1);
                return leftIndex == -1 ? mid : leftIndex; 
            }
            else if (nums[mid] > a)
                return GetFirstIndex(nums, a, start, mid - 1);
            else
                return GetFirstIndex(nums, a, mid + 1, end);

        }
    }
}
