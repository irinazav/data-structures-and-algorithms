﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayNumberOccurrence
{
    class Program
    {
        static void Main(string[] args)
        {
           int[] arr = {1,1,2,2,3,4,5,5,5,6,6,6,6,7,8,9,14,14};
            
            Console.WriteLine("Array:\n{0}\n", String.Join(", ", arr));
            Console.WriteLine("Occurance of number 3 is " + GetOccurrence(3, arr, 0, arr.Length-1));
            Console.WriteLine("Occurance of number 5 is " + GetOccurrence(5, arr, 0, arr.Length - 1));
            Console.WriteLine("Occurance of number 12 is " + GetOccurrence(12, arr, 0, arr.Length - 1));
            Console.ReadKey();
        }

        // based on binary search
        public static int GetOccurrence(int k, int[] numbers, int startIndex, int endIndex)
        {           
            if (endIndex < startIndex)
                return 0;
            
            if (numbers[startIndex] > k)
                return 0;

            if (numbers[endIndex] < k)
                return 0;

            if (numbers[startIndex] == k && numbers[endIndex] == k)
                return endIndex - startIndex + 1;
          
            int midInd = (startIndex + endIndex) / 2;
            if (numbers[midInd] == k)
                return 1 + GetOccurrence(k, numbers, startIndex, midInd - 1) +
                          GetOccurrence(k, numbers, midInd + 1, endIndex);
            else if (numbers[midInd] > k)
                return GetOccurrence(k, numbers, startIndex, midInd - 1);
            else
                return GetOccurrence(k, numbers, midInd + 1, endIndex);
        }
    }
}
