﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayKthLargest
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = new int[20];
		
            Random myRandom = new Random();
		    Console.WriteLine("Numbers: ");
		    for(int i=0; i<nums.Length; i++)
		    {
			    nums[i] = myRandom.Next(100);
                Console.Write(nums[i]);
                if (i < nums.Length - 1) Console.Write(", ");
		    }
		    Console.WriteLine();
		    Console.WriteLine("\n\n10th largest value is "+ FindKthLargest(nums, 10));

		    Console.WriteLine("\n\nArray sorted: ");
            var numsSorted = nums.OrderBy(i => i).ToList();
            for (int i = 0; i < numsSorted.Count(); i++)
		    {
                Console.Write(numsSorted[i]);
                if (i < nums.Length - 1) Console.Write(", ");
		    }
		    Console.WriteLine();
            Console.WriteLine("\n\n10th in sorted array " + numsSorted[9]);
            Console.ReadKey();
        }



        public static int FindKthLargest(int[] nums, int k)
        {
           
            if (k < 1 || k > nums.Length)
                return -1;
            else
                return FindKthLargest(nums, 0, nums.Length - 1, k);
        }

        public static int FindKthLargest(int[] nums, int start, int end, int k)
        {           
            int pivot = start;
            int left = start;
            int right = end;
            while (left <= right)
            {
                
                while (left <= right && nums[left] <= nums[pivot])
                    ++left;
                while (left <= right && nums[right] >= nums[pivot])
                    --right;
                if (left < right)
                {
                    Swap(nums, left, right);
                }
            }
           
            Swap(nums, pivot, right);
            if (k == right + 1)
                return nums[right];
            else if (k > right + 1)
                return FindKthLargest(nums, right + 1, end, k);
            else
                return FindKthLargest(nums, start, right - 1, k);
        }

        private static void Swap(int[] nums, int a, int b)
        {
            int tmp = nums[a];
            nums[a] = nums[b];
            nums[b] = tmp;
        }
    }
}
