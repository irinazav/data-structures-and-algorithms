﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayPrintAllSumPairsForNumber
{
    class Program
    {      
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4, 5, 6, 10 ,12};
            Console.WriteLine("Array:\n{0}\n\nPairs for sum = 7:", String.Join(", ", arr));          
            Print(arr, 7);
            Console.ReadKey();
        }

        public static void Print(int[] sorted, int number)
	    {
		    int left = 0;
		    int right = sorted.Length-1;

		    while(left<right)
		    {
			    int tempSum = sorted[left] + sorted[right];
			    if(tempSum == number)
			    {				  
                    Console.Write("{" + sorted[left] + ", " + sorted[right] + "}");				    
				    left++;
				    right--;
			    }
			    else if(tempSum > number) right--;
			    else  left++;
		    }

	    }
     }
}

