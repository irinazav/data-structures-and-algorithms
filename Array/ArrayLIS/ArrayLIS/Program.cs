﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayLIS
{
    class Program
    {
        static void Main(string[] args)

        {   // Van der Corput sequence, output: 0, 2, 6, 9, 11, 15
            int[] arr1 = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };

            //output: {1, 12, 23, 52, 61, 69, 70}
            int[] arr2 = { 1, 12, 7, 0, 23, 11, 52, 31, 61, 69, 70, 2 };

            Console.Write("Array: {0}\n", String.Join(", ",arr1));
            Console.Write("Longest increasing sequence: {0}\n\n", String.Join(", ", FindLIS(arr1)));

            Console.Write("Array: {0}\n", String.Join(", ", arr2));
            Console.Write("Longest increasing sequence: {0}\n", String.Join(", ", FindLIS(arr2)));

            Console.ReadKey();
            
        }

        //Find longest increasing sequence(LIS)
        public static int[] FindLIS(int[] arr)
        {
            int[] iis = new int[arr.Length];
            int index = 0;
            iis[0] = index;

            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < arr[iis[index]])
                {
                    for (int j = 0; j <= index; j++)
                    {
                        if (arr[i] < arr[iis[j]])
                        {
                            iis[j] = i;
                            break;
                        }
                    }
                }
                else if (arr[i] == arr[iis[index]])
                {

                }
                else
                {
                    iis[++index] = i;
                }
            }

            int[] lis = new int[index + 1];
            lis[index] = arr[iis[index]];

            for (int i = index - 1; i >= 0; i--)
            {
                if (iis[i] < iis[i + 1])
                {
                    lis[i] = arr[iis[i]];
                }
                else
                {
                    for (int j = iis[i + 1] - 1; j >= 0; j--)
                    {
                        if (arr[j] > arr[iis[i]] && arr[j] < arr[iis[i + 1]])
                        {
                            lis[i] = arr[j];
                            iis[i] = j;
                            break;
                        }
                    }
                }
            }

            return lis;
           
        }

    }
}
