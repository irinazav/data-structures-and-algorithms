﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayMaxSubArrayKadane
{
    class Program
    {
        static void Main(string[] args)
        {
            //output: 4, −1, 2, 1
            int[] arr1 = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };

            //output: 5, ‐2, 9
            int[] arr2 = { 1, -3, 5, -2, 9, -8, -6, 4 };

            //output: -2
            int[] arr3 = { -2, -3, -4, -2, -7, -2, -3, -11 };

            Console.WriteLine("Array: {0}", String.Join(", ", arr1));
            MaxSubArray(arr1);

            Console.WriteLine("\nArray: {0}", String.Join(", ", arr2));
            MaxSubArray(arr2);

            Console.WriteLine("\nArray: {0}", String.Join(", ", arr3));
            MaxSubArray(arr3);

            Console.ReadKey();
        }

        public static void MaxSubArray(int[] arr)
        {
            int ans = arr[0],
            ans_l = 0,
            ans_r = 0,
            sum = 0,
            minus_pos = -1;

            for (int r = 0; r < arr.Length; ++r)
            {
                sum += arr[r];
                if (sum > ans)
                {
                    ans = sum;
                    ans_l = minus_pos + 1;
                    ans_r = r;
                }

                if (sum < 0)
                {
                    sum = 0;
                    minus_pos = r;
                }
            }

            // Console.WriteLine("left index= {0} right index= {1} sum = {2}", ans_l, ans_r, sum);
            Console.Write("Maximum subarray: ");
            for (int i = ans_l; i <= ans_r; i++)
            {
                Console.Write("{0}", arr[i]);
                if (i != ans_r) Console.Write(", ");
            }
            Console.Write("\n");
        }

    }
}

