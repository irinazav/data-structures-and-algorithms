﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayBalancePoint
{
    class Program
    {
        //The index where the  sum of the elements to the left it is the same as
        //the sum of the elements to the right of it.
        static void Main(string[] args)
        {
            int[] arr1 = { 1, 2, 3, 7, 6, 5, 9, 5, 6, 7, 5, 2, -1 };//expected 6th position
            int[] arr2 = { -1, 2, 5, 1, 1, -1 };//expected 2
            int[] arr3 = { -1, 4, -5, 2, 1, 1 };//expected 1
            int[] arr4 = { -1, -1, 1, -1, 3 };//expected -1 
            int[] arr5 = { -1, 4, -1, -1, -2 };//expected 0
            int[] arr6 = { 1, -1, 0, 5 };//expected 3


            Console.WriteLine("Balance point for array (expected index = 6)\n{0}\nindex = {1}\n",
                                String.Join(", ", arr1), BalancePoint(arr1));                               
            Console.WriteLine("Balance point for array (expected index = -1)\n{0}\nindex = {1}\n",
                                String.Join(", ", arr4), BalancePoint(arr4));
            Console.WriteLine("Balance point for array (expected index = 0)\n{0}\nindex = {1}\n",
                                String.Join(", ", arr5), BalancePoint(arr5));
            Console.WriteLine("Balance point for array (expected index = 3)\n{0}\nindex = {1}\n",
                                String.Join(", ", arr6), BalancePoint(arr6));
            Console.ReadKey();
        }

        
       
        public static int BalancePoint(int[] a)
        {
            if (a.Length == 0) return -1;
            if (a.Length == 1) return 0;

            int sumLeft = 0;
            for (int i = 0; i < a.Length; i++)
                sumLeft += a[i];

            var sumRight = a[a.Length -1];
                      
            for (int i = 1; i < a.Length; i++)
            {
                if (sumLeft == sumRight) return a.Length  -i;
                sumRight += a[a.Length - 1 - i];
                sumLeft -= a[a.Length - i];
                
            }
            if (sumLeft == sumRight) return 0;
            return -1;
        }

    }
}
