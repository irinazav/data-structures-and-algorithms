﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeanarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
           
            BSTNode t = new BSTNode(7);
            t.left = new BSTNode(4); t.right = new BSTNode(10);
            t.left.left = new BSTNode(3); t.left.right = new BSTNode(5);
            t.right.left = new BSTNode(9); t.right.right = new BSTNode(11);

            //Traverse tree
            Console.WriteLine("InOrder recursive: ");
            t.InOrderRecursive();
            Console.WriteLine();

		    Console.WriteLine("\nInOrder iterative: ");
            t.InOrderIterative();
            Console.WriteLine();

            Console.WriteLine("\nPreOrder recursive: ");
            t.PreOrderRecursive();
            Console.WriteLine();

            Console.WriteLine("\nPreOrder iterative: ");
            t.PreOrderIterative();
            Console.WriteLine();

            Console.WriteLine("\nPostOrder recursive: ");
            t.PostOrderRecursive();
            Console.WriteLine();

            Console.WriteLine("\nPostOrder iterative: ");
            t.PostOrderIterative();
            Console.WriteLine();

            //Add, delete values
            t.Add(1);
            Console.WriteLine("\nAdded value = 1, print postorder: ");
            t.PostOrderIterative();

            t.Remove(3, t);
            Console.WriteLine("\n\nRemoved value = 3, print postorder: ");
            t.PostOrderIterative();

            //Max, Min
            Console.WriteLine("\n\nMin value: {0} Max value: {1}", t.MinValue(), t.MaxValue());

            //Search value
            Console.WriteLine("\nIf exists: value 89 - {0}, value 5 - {1}", t.Search(89), t.Search(5));

            //Tree level queue-based
            Console.WriteLine("\nQueue-based method - 3rd Level:");
            BSTNode.PrintTreeLevel(t, 2);
            Console.WriteLine();

            Console.WriteLine("Queue-based method - 2rd Level:");
            BSTNode.PrintTreeLevel(t, 1);
            Console.WriteLine();

            //Tree level recursive 
            Console.WriteLine("\nRecursive method - 3rd Level is ");
            BSTNode.PrintTreeLevel(t, 0, 2);
            Console.WriteLine("\nRecursive method - 2rd Level is ");
            BSTNode.PrintTreeLevel(t, 0, 1);
            Console.WriteLine();
            Console.WriteLine();

            //Is tree BST
            Console.WriteLine("BTree is balanced? {0}", BSTNode.IsBST(t));
            Console.WriteLine();

            // Find the closest value in a BST with a given value     
            BSTNode myBST = new BSTNode(100);
            myBST.left = new BSTNode(50);
            myBST.right = new BSTNode(200);
            myBST.left.left = new BSTNode(10);
            myBST.left.right = new BSTNode(60);
            myBST.right.left = new BSTNode(150);
            myBST.right.right = new BSTNode(300);

            Console.WriteLine("Test tree for closest value, print inorder: ");
            myBST.InOrderRecursive();
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Closest value in BST to 120 is {0} with difference {1}\n",
            BSTNode.getClosetTreeNode(myBST, 120),
            BSTNode.MinDifference(myBST, 120));

            Console.WriteLine("Closest value in BST to 80 is {0} with difference {1}\n",
            BSTNode.getClosetTreeNode(myBST, 80),
            BSTNode.MinDifference(myBST, 80));

            Console.WriteLine("Closest value in BST to 1000 is {0} with difference {1}\n",
            BSTNode.getClosetTreeNode(myBST, 1000),
            BSTNode.MinDifference(myBST, 1000));

            Console.WriteLine("Closest value in BST to 1000 is {0} with difference {1}\n",
            BSTNode.getClosetTreeNode(myBST, 0),
            BSTNode.MinDifference(myBST, 0));
            Console.WriteLine();

            Console.ReadKey();
        }                

        
        public class BSTNode
        {

            public int value;
            public BSTNode left;
            public BSTNode right;

            
            public BSTNode(int value)
            {
                this.value = value;
                left = null;
                right = null;
            }

            public bool Search(int value)
            {
                if (value == this.value)
                    return true;

                else if (value < this.value)
                {
                    if (left == null)
                        return false;
                    else
                        return left.Search(value);
                }

                else if (value > this.value)
                {
                   if (right == null)
                        return false;
                    else
                        return right.Search(value);
                }
                return false;
            }

          
            public bool Add(int value)
            {
                if (value == this.value)
                    return false;

                else if (value < this.value)
                {
                    if (left == null)
                    {
                        left = new BSTNode(value);
                        return true;
                    }
                    else
                        return left.Add(value);

                }
                else if (value > this.value)
                {
                    if (right == null)
                    {
                        right = new BSTNode(value);
                        return true;
                    }
                    else
                        return right.Add(value);
                }
                return false;

            }
            
            public bool Remove(int value, BSTNode parent)
            {
                if (value < this.value)
                {
                    if (left != null)
                        return left.Remove(value, this);
                    else
                        return false;
                }

                else if (value > this.value)
                {
                    if (right != null)
                        return right.Remove(value, this);
                    else
                        return false;
                }
                else
                {
                    if (left != null && right != null)
                    {
                        this.value = right.MinValue();
                        right.Remove(this.value, this);
                    }

                    else if (parent.left == this)
                    {
                        parent.left = (left != null) ? left : right;
                    }

                    else if (parent.right == this)
                    {
                        parent.right = (left != null) ? left : right;
                    }

                    return true;
                }
            }


          
            public int MinValue()
            {
                if (left == null)
                    return value;

                else
                    return left.MinValue();
            }

            public int MaxValue()
            {
                if (right == null)
                    return value;

                else
                    return right.MaxValue();
            }


            private static int IsBalanced(BSTNode root)
            {
                if (root == null) return 0;

                int leftHeight = IsBalanced(root.left);
                if (leftHeight == -1)  return -1;

                int rightHeight = IsBalanced(root.right);
                if (rightHeight == -1) return -1;

                if (Math.Abs(leftHeight - rightHeight) > 1) return -1;
                else return Math.Max(leftHeight, rightHeight) + 1;
            }


            public static bool IsBST(BSTNode root)
            {
                return IsBalanced(root) != -1;
            }


            public static int getClosetTreeNode(BSTNode t, int v)
            {
                if (t == null) throw new ArgumentNullException();
              
                int closetTreeNodeValue = 0;
                int minDist = int.MaxValue;

                while (true)
                {
                    if (t == null) break;

                    if (t.value == v)
                    { 
                        return v;
                    }

                    else if (t.value < v)
                    {

                        if ((v - t.value) < minDist)
                        {
                            minDist = v - t.value;
                            closetTreeNodeValue = t.value;
                        }
                        t = t.right;
                    }

                    else
                    { 
                        if ((t.value - v) < minDist)
                        {
                            minDist = t.value - v;
                            closetTreeNodeValue = t.value;
                        }
                        t = t.left;
                    }
                }

                return closetTreeNodeValue;

            }


            public static int MinDifference(BSTNode t, int v)
            {
               
                if (t == null)
                    return int.MaxValue;
               
                if (t.value < v)
                    return smallestDiff(t.value - v, MinDifference(t.right, v)); 
                else
                    return smallestDiff(t.value - v, MinDifference(t.left, v));
            }


            private static int smallestDiff(int a, int b)
            {
                if (Math.Abs(a) > Math.Abs(b))
                    return b;
                return a;
            }

           
            public void PreOrderRecursive()
            {
                Console.Write("{0} ", value);
                if (left != null)
                    left.PreOrderRecursive();

                if (right != null)
                    right.PreOrderRecursive();
            }

            public void PreOrderIterative()
	        {
		      
			    Stack<BSTNode> myStack = new Stack<BSTNode>();
		        BSTNode current = this;
		        while(current!=null || !(myStack.Count == 0))
		        {			       
			        if(current!=null)
			        {
                        Console.Write("{0} ", current.value);
				        myStack.Push(current.right);
				        current = current.left;
			        }
			        else
			        {
				        current = myStack.Pop();
			        }
		        }
	        }


            public void InOrderRecursive()
	        {
		        if(left!=null)
                    left.InOrderRecursive();
                  Console.Write("{0} ", value);
		        if(right!=null)
                    right.InOrderRecursive();
	         }


            public void InOrderIterative()
	        {
		        Stack<BSTNode> myStack = new Stack<BSTNode>();		
		        BSTNode current = this;

		        while(current!=null || !(myStack.Count == 0))
		        {			
			        if(current!=null)
			        {
				        myStack.Push(current);
				        current = current.left;
			        }
			        else
			        {
				        current = myStack.Pop();
                        Console.Write("{0} ", current.value);
				        current = current.right;
			        }
		        }
	        }


            public void PostOrderRecursive()
             {
                 if (left != null)
                     left.PostOrderRecursive();
                 if (right != null)
                     right.PostOrderRecursive();
                 Console.Write("{0} ", value);
             }


            public void PostOrderIterative() 
            {
		        Stack<BSTNode> treeNodes = new Stack<BSTNode>();
		        BSTNode previous = new BSTNode(1000);
		        treeNodes.Push(this);
		
		        while(!(treeNodes.Count == 0)) {
			        BSTNode current = treeNodes.Peek();

			        if(current == null) 
				        treeNodes.Pop();
			
			        else if(current.left==null && current.right==null) {
                        Console.Write("{0} ", current.value);
				        treeNodes.Pop();
			        }
			
			        else if(current.left == previous)
				        treeNodes.Push(current.right);
			
			        else if(current.right == previous) {
				         Console.Write("{0} ", current.value);
				        treeNodes.Pop();
			        }
			
			        else
				        treeNodes.Push(current.left);
			
			        previous = current;
		        }
	        }


            public static void PrintTreeLevel(BSTNode t, int currentLevel, int level)
	        {
                if (t == null || currentLevel > level)
			        return;

                if (currentLevel == level)			
                         Console.Write("{0} ", t.value);
		        else
		        {
                    PrintTreeLevel(t.left, currentLevel + 1, level);
                    PrintTreeLevel(t.right, currentLevel + 1, level);
		        }
	        }


            public static void PrintTreeLevel(BSTNode t, int level)
            {
                if (level < 0) return;

                    Queue<BSTNode> trees = new Queue<BSTNode>();
                    Queue<int> levels = new Queue<int>();
                    trees.Enqueue(t);
                    levels.Enqueue(0);

                    while (!(trees.Count == 0))
                    {
                        BSTNode temp = trees.Dequeue();
                        int currentLevel = levels.Dequeue();

                        if (temp == null) return;

                        else if (currentLevel == level)
                            Console.Write("{0} ", temp.value);

                        else
                        {
                            trees.Enqueue(temp.left); levels.Enqueue(currentLevel + 1);
                            trees.Enqueue(temp.right); levels.Enqueue(currentLevel + 1);
                        }
                    }
                }
           }           
      }
  }
